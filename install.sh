#!/bin/bash

# check for update
sudo apt-get update
# install python3
sudo apt-get -y install python3

# create new virtual environment
python3 -m venv venv
# activate venv
source venv/bin/activate
# install dependencies
python -m pip install -r requirements.txt

# copy geckodriver into the PATH
sudo cp -f geckodriver /usr/local/bin
