import json

import txredisapi
from scrapy.exceptions import NotConfigured
from scrapy.utils.project import get_project_settings
from scrapy.crawler import Crawler
from scrapy.spiders import Spider
from twisted.internet import defer

from basicplatform.items import Item


class RedisCache:
    @classmethod
    def from_crawler(cls, crawler: Crawler):
        """
        Scrapy calls this class method only once on the beginning.

        Args:
            crawler: Crawler class - caller.

        Returns:
            Newly initialized RedisCache object.
        """
        redis_url = crawler.settings.get('REDIS_PIPELINE_URL')

        # If 'redis_url' does not exist, abort.
        if not redis_url:
            raise NotConfigured

        parsed = redis_url.split(':')

        return cls(crawler, parsed[0], int(parsed[1]))

    @defer.inlineCallbacks
    def init_redis__pool(self, host: str, port: int):
        """
        Args:
            host: Hostname of redis database.
            port: Port of redis database.
        """
        self.connection = yield txredisapi.lazyConnectionPool(host, port)

    def __init__(self, crawler, host, port):
        """
        Args:
            host: Hostname of redis database.
            port: Port of redis database.
        """
        self.connection = None
        self.init_redis__pool(host, port)

    @defer.inlineCallbacks
    def process_item(self, items: Item, spider: Spider):
        """Store items into the redis asynchronously.
        Used queue as redis data type.

        Args:
            items: String containing json formatted items.
        """
        items_json = dict(items)

        # Store items only if marks have been found.
        if items_json['regex_match'] != ['']:
            fields_json = json.dumps(items_json)
            yield self.connection.rpush(get_project_settings().get('REDIS_QUEUE'), fields_json)

        defer.returnValue(items)
