from scrapy.item import Item
from scrapy.item import Field


class BasicplatformItem(Item):
    """
    Attributes:
        url (string): Url of currently scraped page.
        job (string): Name of job that is running.
        spider (string): Name of scrapy spider.
        server (string): Name of server the scrapy is running on.
        date (string): Current date and time.
        regex_pattern (string): Regex pattern.
        regex_match (string): Regex matches.
        headers (string): Headers used with current scraped page.
        htmlcontent (string): Html of currently scraped page.
    """
    # Housekeeping fields
    url    = Field()
    job    = Field()
    spider = Field()
    server = Field()
    date   = Field()
    # Primary fields
    regex_pattern = Field()
    regex_match   = Field()
    headers       = Field()
    htmlcontent   = Field()
