import datetime
import socket
import re

from scrapy.loader import ItemLoader
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Response
from scrapy.linkextractors import LinkExtractor
from scrapy.utils.project import get_project_settings

from basicplatform.items import BasicplatformItem


class BasicSpider(CrawlSpider):
    """Spider class for scraping/crawling.
    Basic scenario without login or such other mechanisms.
    If regex matches, save the matches.
    """
    settings = get_project_settings()

    name = 'basic'
    allowed_domains = settings.getlist('ALLOWED_DOMAINS')
    start_urls = settings.getlist('START_URLS')

    pattern = re.compile(settings.get('PATTERN'), re.IGNORECASE | re.MULTILINE)

    # Rules for crawling.
    rules = (
        Rule(LinkExtractor(), callback='parse_item', follow=True),
    )

    def parse_item(self, response: Response) -> ItemLoader:
        """This function is called as callback to parse each scraped page.

        Args:
            response: Scraped page.

        Returns:
            Instance of item loader with all filled fields.
        """
        # Create the item loader using the response.
        loader = ItemLoader(item=BasicplatformItem(), response=response)

        # Housekeeping fields.
        loader.add_value('url'   , response.url)
        loader.add_value('job'   , self.settings.get('JOB'))
        loader.add_value('spider', self.name)
        loader.add_value('server', socket.gethostname())
        loader.add_value('date'  , datetime.datetime.now().isoformat(' '))

        # Primary fields.
        loader.add_value('headers'    , str(response.headers))
        loader.add_value('htmlcontent', str(response.text))
        
        matches = set(re.findall(pattern=BasicSpider.pattern, string=response.text))
        matches_str = ''
        for match in matches:
            matches_str += match + ';'

        loader.add_value('regex_match', matches_str)
        loader.add_value('regex_pattern', get_project_settings().get('PATTERN'))

        return loader.load_item()
