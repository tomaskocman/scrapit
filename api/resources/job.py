import os, sys
import re
import subprocess
import sys
from enum import Enum, unique, auto

from flask import request
from flask_restful import Resource

import settings
from models.job import CreateJobModel
from utils import Logger
from utils import deserialization


def set_value(settings_path: str, l_value: str, r_value: str):
    """Modifies regex pattern in scrapy config file.

    Args:
        settings_path: Path to settings.py.
        l_value: Left side of assignment.
        r_value: Value to be assigned.
    """
    if not os.path.isfile(settings_path):
        raise RuntimeError(f'File {settings_path} does not exist.')

    inputfile_text = open(settings_path, 'r').readlines()
    outputfile_text = []
    for line in inputfile_text:
        if line.startswith(l_value):
            outputfile_text.append(f"{l_value} = {r_value}\n")
        else:
            outputfile_text.append(line)

    open(settings_path, 'w').writelines(outputfile_text)


def set_selenium(settings_path: str, selenium_mode: bool):
    """Sets|Unsets selenium middleware.
    Selenium middleware is set only if it is uncommented.

    Args:
        settings_path: Path to settings.py.
        selenium_mode: True if selenium middleware is going to be set, False otherwise.
    """
    if not os.path.isfile(settings_path):
        raise RuntimeError(f'File {settings_path} does not exist.')

    inputfile_text = open(settings_path, 'r').readlines()
    outputfile_text = []
    edit_mode = False
    for line in inputfile_text:
        if edit_mode or re.search('DOWNLOADER_MIDDLEWARES', line):
            # Are we still in edit area?
            if line == '\n':
                edit_mode = False
                outputfile_text.append(line)
            else:
                edit_mode = True
                if selenium_mode:
                    if line.startswith('#'):
                        outputfile_text.append(line[1:])
                    else:
                        outputfile_text.append(line)
                else:
                    if line.startswith('#'):
                        outputfile_text.append(line)
                    else:
                        outputfile_text.append('#' + line)
        else:
            outputfile_text.append(line)

    open(settings_path, 'w').writelines(outputfile_text)


@unique
class ProcessStatus(Enum):
    """Enum class for distinction of job states."""
    IN_PROGRESS     = auto()
    UNKNOWN_PROCESS = auto()
    DUPLICATION     = auto()
    DONE            = auto()


class JobsStorage:
    """Class for job logic. Saves and manipulates particular job.
    
    Class variables:
        jobs: Dict storage of processes.
        
    JobsStorage supports operations for:
        Inserting a new job,
        finding job with particular name,
        checking status of existing job,
        getting names of all running jobs,
        stopping the running job,
        stopping of all running jobs.
    """
    logger = Logger.get_logger('JobsStorage')
    jobs = {}

    @staticmethod
    def add_job(job_name: str, job_process: subprocess.Popen) -> ProcessStatus:
        """Inserts a new job into the storage.
        
        If there is some process with the same name, replace it.
        It is responsibility for user to check, if there is a job with the same name.
        
        Args:
            job_name: Name of newly created job.
            job_process: Instance of subprocess.
            
        Returns:
            Result of add operation.
        """
        # Actualize state of process.
        # If there is job with the same name but completed, remove it.
        JobsStorage.check_job_status(job_name)
        JobsStorage.jobs[job_name] = job_process

        return ProcessStatus.DONE

    @staticmethod
    def find_job(job_name: str) -> bool:
        """Finds a running job.
        
        Args:
            job_name: Name of running job.
            
        Returns:
            True if the job is running, False otherwise.
        """
        if job_name in JobsStorage.jobs:
            return True

        return False

    @staticmethod
    def check_job_status(job_name: str) -> (ProcessStatus, int):
        """Checks status of existing job.
        
        Status might be:
            ProcessStatus.UNKNOWN_PROCESS - process with that name does not exist.
            ProcessStatus.IN_PROGRESS     - process is running.
            ProcessStatus.DONE            - process is completed.
            
        Args:
            job_name: Name of a job to check.
            
        Returns:
            Job status and return code if job has ended.
        """
        process = JobsStorage.jobs.get(job_name)
        if not process:
            return ProcessStatus.UNKNOWN_PROCESS, -1

        status = process.poll()
        if status is None:
            return ProcessStatus.IN_PROGRESS, -2
        else:
            ret_code = JobsStorage.jobs[job_name].returncode
            del JobsStorage.jobs[job_name]

        return ProcessStatus.DONE, ret_code

    @staticmethod
    def get_all_jobs() -> list:
        """Gets names of all running jobs.
        
        Returns:
            List of names of all running jobs.
        """
        jobs = []
        # Iterate through copy of list in order to possibly delete completed jobs from the original list.
        for job_name in list(JobsStorage.jobs):
            process = JobsStorage.jobs.get(job_name)
            if not process:
                continue

            status = process.poll()
            if status is None:
                jobs.append(job_name)
            else:
                # Delete job from the original list storage.
                del JobsStorage.jobs[job_name]

        return jobs

    @staticmethod
    def stop_job(job_name: str) -> ProcessStatus:
        """Stops the running job.
        
        Returns:
            ProcessStatus.UNKNOWN_PROCESS if the job does not exist,
            ProcessStatus.DONE otherwise.
        """
        # Actualize state of process.
        # If there is job with the same name but completed, remove it.
        JobsStorage.check_job_status(job_name)

        process = JobsStorage.jobs.get(job_name)
        if not process:
            return ProcessStatus.UNKNOWN_PROCESS

        JobsStorage.jobs[job_name].terminate()
        del JobsStorage.jobs[job_name]

        return ProcessStatus.DONE

    @staticmethod
    def stop_all_jobs():
        """Stops all running jobs."""
        for job_name in dict(JobsStorage.jobs):
            JobsStorage.stop_job(job_name)


class Job(Resource):
    """Class for manipulating with a single job"""
    logger = Logger.get_logger('Job')

    def get(self, job):
        """Gets status of particular job.
        
        Status might be:
            ProcessStatus.UNKNOWN_PROCESS - process with that name does not exist.
            ProcessStatus.IN_PROGRESS     - process is running.
            ProcessStatus.DONE            - process is completed.
        """
        status, ret_code = JobsStorage.check_job_status(job)
        if status is ProcessStatus.UNKNOWN_PROCESS:
            return {'message': f'process {job} not found'}, 404
        elif status is ProcessStatus.IN_PROGRESS:
            return {'message': f'process {job} is in progress'}, 200
        elif status is ProcessStatus.DONE:
            return {'message': f'process {job} ended with return code {ret_code}'}, 200

        return {}, 500

    def delete(self, job):
        """Deletes particular job.
        
        If the job does not exist, return not found.
        """
        if JobsStorage.stop_job(job) is ProcessStatus.UNKNOWN_PROCESS:
            return {}, 404
        else:
            return {}, 204


class Jobs(Resource):
    """Class for manipulating with all running jobs."""
    logger = Logger.get_logger('Jobs')

    def get(self):
        """Retrieves names of all running jobs."""
        jobs = JobsStorage.get_all_jobs()
        return {'jobNames': jobs}, 200

    @deserialization(CreateJobModel())
    def post(self, job=None):
        """Creates new job - starts new process.

        If the job is already running, return message about duplicate.
        Incoming JSON has to contain name of new job and name of scrapy spider to run.
        """
        logger = Logger.get_logger('Job')

        message = request.get_json()
        if JobsStorage.find_job(message['job']):
            return {'message': f"process {message['job']} is already running"}, 409

        os.chdir(settings.SCRAPY_PROJECT_PATH)

        # Set mode for JS.
        if message['dynamicJavaScript'] == 'True':
            set_selenium('basicplatform/settings.py', True)
        else:
            set_selenium('basicplatform/settings.py', False)
        # Set robotstxt_obey.
        set_value('basicplatform/settings.py', 'JOB', "'" + message['job'] + "'")
        # Set regex pattern.
        set_value('basicplatform/settings.py', 'PATTERN', "r'" + message['regexPattern'] + "'")
        # Set log level.
        set_value('basicplatform/settings.py', 'LOG_LEVEL', "'" + message['logLevel'] + "'")
        # Set log file.
        set_value('basicplatform/settings.py', 'LOG_FILE', "'" + message['logFile'] + "'")
        # Set robotstxt_obey.
        set_value('basicplatform/settings.py', 'ROBOTSTXT_OBEY', message['robotstxtObey'])
        # Set allowed domains.
        set_value('basicplatform/settings.py', 'ALLOWED_DOMAINS', message['allowedDomains'])
        # Set start urls.
        set_value('basicplatform/settings.py', 'START_URLS', message['startUrls'])

        if message['spider'] == 'basic':
            process = subprocess.Popen([
                f'{sys.exec_prefix}/bin/scrapy',
                'crawl',
                message['spider']
            ])
        elif message['spider'] == 'login':
            mandatory_keys = ['loginUrl', 'formName', 'formLogin', 'formPassword', 'username', 'password']
            for key in mandatory_keys:
                if key not in message.keys():
                    logger.error('Some field is missing')
                    return {'message': 'some field is missing'}, 400

            # Set login URL.
            set_value('basicplatform/settings.py', 'LOGIN_URL', "'" + message['loginUrl'] + "'")
            # Set form name.
            set_value('basicplatform/settings.py', 'FORM_NAME', "'" + message['formName'] + "'")
            # Set form login.
            set_value('basicplatform/settings.py', 'FORM_LOGIN', "'" + message['formLogin'] + "'")
            # Set form password.
            set_value('basicplatform/settings.py', 'FORM_PASSWORD', "'" + message['formPassword'] + "'")

            process = subprocess.Popen([
                f'{sys.exec_prefix}/bin/scrapy',
                'crawl',
                message['spider'],
                '-a',
                f'username={message["username"]}',
                '-a',
                f'password={message["password"]}'
            ])
        else:
            logger.error('Unknown spider')
            return {'message': 'unknown spider'}, 400

        JobsStorage.add_job(message['job'], process)

        return {}, 201

    def delete(self):
        """Deletes all running jobs"""
        JobsStorage.stop_all_jobs()
        return {}, 200
