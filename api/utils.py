import logging
from functools import wraps

from flask import request
from flask_marshmallow import Marshmallow

import settings


ma = Marshmallow()


class Logger:
    @staticmethod
    def get_logger(name: str) -> logging.Logger:
        """Configures logger to be used in the module where is imported after calling 'get_logger'.
        
        Adds two handlers to basic logger. One for file, second for console.
        For each handler sets format of logs and log level.
        
        Args:
            name: Name of module the logger will be initialized with.
            
        Returns:
            Configured logging.Logger object.
        """
        logger = logging.getLogger(name)

        if not logger.handlers:
            formatter = logging.Formatter('%(asctime)s | %(name)-15s | %(levelname)-7s | %(message)s',
                                          datefmt='%Y-%m-%d %H:%M:%S')

            file_handler = logging.FileHandler(settings.LOG_PATH)
            file_handler.setFormatter(formatter)
            file_handler.setLevel(settings.LOG_FILE_LEVEL)

            console_handler = logging.StreamHandler()
            console_handler.setFormatter(formatter)
            console_handler.setLevel(settings.LOG_CONSOLE_LEVEL)

            logger.addHandler(file_handler)
            logger.addHandler(console_handler)

            logger.setLevel(logging.DEBUG)

            return logger


def deserialization(schema: ma.Schema):
    """Wraps method and checks whether provided json data corresponds to the model.
    
    Args:
        schema: Schema to load the data and check correctness.
        
    Returns:
        Decorated function.
    """
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            json_data = request.get_json()
            if not json_data:
                return {'message': 'no input data provided'}, 400

            data, errors = schema.load(json_data)
            if errors != {}:
                return {'message': 'bad json'}, 400

            return f(*args, **kwargs)
        return wrapper
    return decorator
