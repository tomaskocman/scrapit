from flask import Flask
from flask_restful import Api
from flask.logging import default_handler
from flask_cors import CORS

import logging
import settings

werkzeug_logger       = logging.getLogger('werkzeug')
werkzeug_file_handler = logging.FileHandler(settings.LOG_PATH)
werkzeug_logger.addHandler(werkzeug_file_handler)

app = Flask(__name__)
CORS(app)
api = Api(app)

app.logger.removeHandler(default_handler)

from resources.job import Job, Jobs

api.add_resource(Job,  '/jobs/<string:job>')
api.add_resource(Jobs, '/jobs')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
