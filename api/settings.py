import logging

SCRAPY_PROJECT_PATH = '/root/scrapit/basicplatform'

LOG_PATH          = '/tmp/scrapy_web_api.log'
LOG_CONSOLE_LEVEL = logging.DEBUG
LOG_FILE_LEVEL    = logging.INFO
