from flask_marshmallow.fields import fields

from utils import ma


class CreateJobModel(ma.Schema):
    """Model for converting incoming JSON to Python dict and check correctness.

    TODO: username and password pass by parameter on cmd
    """
    robotstxtObey     = fields.Bool(required=True)
    dynamicJavaScript = fields.Bool(required=True)
    job               = fields.Str(required=True)
    spider            = fields.Str(required=True)
    regexPattern      = fields.Str(required=True)
    logLevel          = fields.Str(required=True)
    logFile           = fields.Str(required=True)
    loginUrl          = fields.Str(required=False)
    formName          = fields.Str(required=False)
    formLogin         = fields.Str(required=False)
    formPassword      = fields.Str(required=False)
    username          = fields.Str(required=False)
    password          = fields.Str(required=False)
    allowedDomains    = fields.List(fields.Str, required=True)
    startUrls         = fields.List(fields.Str, required=True)
