FROM python:3.6-slim

EXPOSE 5000

COPY . /root/scrapit/
COPY geckodriver /usr/local/bin/

WORKDIR /root/scrapit/

RUN apt update && \
	apt install -y gcc vim && \
	python -m venv venv && \
	. venv/bin/activate && \
	python -m pip install -r requirements.txt && \
	echo 'alias ll="ls -all"' >> ~/.bashrc

CMD . venv/bin/activate;python api/app.py

