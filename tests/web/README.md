# Scrapy web test

This web serves for testing purposes. Content of the web is automatically generated.<br/>
The Web perfectly suits for demonstration of testing Scrapy spiders.<br/>
The Project *testplatform* serves to explain the testing of platform Scrapy spiders.<br/>
<br/>

### Steps to run web server
```
> source $PROJ_ROOT/venv/bin/activate
> python tests/web/web.py &
```
Web will run on localhost:9312 by default. To change port, modify file *web.py*.
