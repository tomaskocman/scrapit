import datetime
import socket
from urllib.parse import urljoin

from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, Join
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Response
from scrapy.linkextractors import LinkExtractor
from scrapy.utils.project import get_project_settings

from testplatform.items import TestplatformItem


class TestBasicSpider(CrawlSpider):
    settings = get_project_settings()

    name = 'test_basic'
    allowed_domains = settings.getlist('ALLOWED_DOMAINS')
    start_urls = settings.getlist('START_URLS_BASIC')

    # Rules for crawling.
    rules = (
        Rule(LinkExtractor(), callback='parse_item', follow=True),
    )

    def parse_item(self, response: Response) -> ItemLoader:
        """This function parses a property page.

        @url http://localhost:9312/properties/property_000000.html
        @returns items 1
        @scrapes title price description address image_urls
        @scrapes url project spider server date
        """
        # Create the loader using the response.
        l = ItemLoader(item=TestplatformItem(), response=response)

        # Load fields using XPath expressions.
        l.add_xpath(
            'title',
            '//*[@itemprop="name"][1]/text()',
            MapCompose(str.strip, str.title)
        )
        l.add_xpath(
            'price',
            './/*[@itemprop="price"][1]/text()',
            MapCompose(lambda i: i.replace(',', ''), float),
            re='[,.0-9]+'
        )
        l.add_xpath(
            'description',
            '//*[@itemprop="description"][1]/text()',
            MapCompose(str.strip), Join()
        )
        l.add_xpath(
            'address',
            '//*[@itemtype="http://schema.org/Place"][1]/text()',
            MapCompose(str.strip))
        l.add_xpath(
            'image_urls',
            '//*[@itemprop="image"][1]/@src',
            MapCompose(lambda i: urljoin(response.url, i))
        )

        # Housekeeping fields.
        l.add_value('url', response.url)
        l.add_value('project', self.settings.get('BOT_NAME'))
        l.add_value('spider', self.name)
        l.add_value('server', socket.gethostname())
        l.add_value('date', datetime.datetime.now())

        return l.load_item()
