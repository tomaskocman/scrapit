# Scrapy
<img src="https://cdn-images-1.medium.com/max/1600/1*FsyqhfN3evDrckB5IH_h8w.png" width=150 height=150><br/>
<br/>
#### Authors: Bc. Tomáš Kocman, Ing. Libor Polčák, Ph.D.<br/>
**Disclaimer**: Scrapy is a part of whole platform. It is not a standalone application!<br/>

Scrapy serves as a web crawler/scraper. It crawls through the whole web and analyzes individual pages.<br/>
**Input for crawling:** informations in settings.py.<br/>
**Output of crawling:** JSON objects stored in Redis database.

### Steps to run Scrapy unit tests
```
> source venv/bin/activate
> python tests/web/web.py &
> cd tests/testplatform
> scrapy check
```

### Steps to run REST API
**Note**: instead of running scrapy crawler directly, it is convenient to run REST API that controlls scrapy processes.
```
> Ensure that Redis is running!
> source venv/bin/activate
> python api/app.py
```
